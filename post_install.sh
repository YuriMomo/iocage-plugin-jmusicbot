#!/bin/sh -x
echo "Creating the folder structure"
mkdir -p /usr/local/JmusicBot


echo "Downloading the JmusicBot"
LOCATION=$(curl -s https://api.github.com/repos/jagrosh/MusicBot/releases/latest \
| grep "zipball_url" \
| awk '{ print $2 }' \
| sed 's/,$//'       \
| sed 's/"//g' )     \
; curl -L -o /usr/local/JmusicBot/JmusicBot.jar $LOCATION


echo "Adding new user"
pw useradd JmusicBot -u 333 -c "JmusicBot daemon user" -d /nonexistent -s /sbin/nologin


echo "Updating permission"
chown -R JmusicBot:nogroup /usr/local/JmusicBot

# Enable the service
sysrc JmusicBot_enable=YES
sysrc JmusicBot_user=JmusicBot

echo "Starting the service"
service JmusicBot start
sleep 3


JmusicBot_pid=$(pgrep java)
echo $JmusicBot_pid > /usr/local/JmusicBot/JmusicBot.pid

chown JmusicBot:nogroup /usr/local/JmusicBot/JmusicBot.pid


echo "JmusicBot now installed" 

